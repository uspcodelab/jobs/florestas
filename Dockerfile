FROM node:alpine

WORKDIR /usr/src

EXPOSE 8080

RUN apk update && apk add yarn python g++ make && rm -rf /var/cache/apk/*

COPY package.json package-lock.json ./

RUN npm install

COPY . .

CMD npm run serve