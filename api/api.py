from flask import Flask
from flask import app
from flask import json
from flask import jsonify
from flask import request
from flask_cors import CORS

import os
import sys
import shutil
import zipfile

# Models
sys.path.append('./src')
from classify import classify
from preprocess import preprocess

# Flask server
app = Flask(__name__)
CORS(app)

codes = {
    "admission_assessment": os.environ["ADMISSION_ASSESSMENT_CODE"], # "QA",
    "follow_up_assessment": os.environ["FOLLOW_UP_ASSESSMENT_CODE"], # "QF",
    "surgical_evaluation": os.environ["SURGICAL_EVALUATION_CODE"], #"QS"
}

dirs = {
    "models": os.environ["MODELS_DIR"],
    "data": os.environ["DATA_DIR"],
    "metadata": os.environ["METADATA_DIR"],
    "accuracy_threshold": os.environ["ACCURACY_THRESHOLD_DIR"],
    "feature_contributions": os.environ["FEATURE_CONTRIBUTIONS_DIR"],
    "output": os.environ["OUTPUT_DIR"],
}

# Routes
@app.route("/field_descriptions")
def field_descriptions():
    json_file = os.path.join(dirs['metadata'],
                             f"metadata_field_descriptions.json")

    with open(json_file) as f:
        d = json.load(f)
        return jsonify(d)

@app.route("/<model_category>/field_relevance")
def field_relevance(model_category):
    json_file = os.path.join(dirs['accuracy_threshold'],
                             f"{model_category}_field_relevance.json")

    with open(json_file) as f:
        d = json.load(f)
        return jsonify(d)

@app.route("/<model_category>/accuracy_threshold")
def accuracy_threshold(model_category):
    json_file = os.path.join(dirs['accuracy_threshold'],
                             f"{model_category}.json")

    with open(json_file) as f:
        d = json.load(f)
        return jsonify(d)

@app.route("/<model_category>/<selected>/feature_contributions")
def feature_contribution(model_category, selected):
    json_file = os.path.join(dirs['feature_contributions'],
                             f"{model_category}_{selected}.json")

    with open(json_file) as f:
        d = json.load(f)
        return jsonify(d)

@app.route("/<model_category>/<selected>/classify", methods=['POST'])
def classification(model_category, selected):
    user_id = request.json.get("user_id")
    if user_id is None:
        return jsonify({ "error": "No user_id defined" }), 400

    export_id = request.json.get("export_id")
    if export_id is None:
        return jsonify({ "error": "No export_id defined" }), 400

    app.logger.info(f"Classifying export {export_id} for user {user_id}")

    paths = {
        "metadata": dirs['metadata'],
        "model": os.path.join(dirs['models'],
            f"{model_category}_{selected}.pickle"),
        "nes": os.path.join(dirs['data'],
            f"{user_id}/{export_id}"),
        "zip": os.path.join(dirs['data'],
            f"{user_id}/{export_id}/export.zip"),
        "data": os.path.join(dirs['data'],
            f"{user_id}/{export_id}/data"),
        "preprocessed": os.path.join(dirs['output'],
            f"preprocessings/{export_id}"),
        "classifications": os.path.join(dirs['output'],
            f"{user_id}/{model_category}/{selected}/{export_id}"),
    }

    try:
        if not os.path.isdir(paths['data']):
            with zipfile.ZipFile(paths['zip'], 'r') as zip_ref:
                zip_ref.extractall(paths['nes'])
    except Exception as e:
        return jsonify({ "error": f"Unzipping failed: {e}" }), 500

    if not os.path.isdir(paths['preprocessed']):
        app.logger.info(f"Preprocessing export {export_id}")
        os.makedirs(paths['preprocessed'])

        try:
            preprocess(path=paths['data'],
                       output_path=paths['preprocessed'],
                       metadata_path=paths['metadata'],
                       main_questionnaire=codes["admission_assessment"],
                       class_questionnaire=codes["follow_up_assessment"],
                       surgery_questionnaire=codes["surgical_evaluation"],
                       operation=1,
                       not_applicable=True,
                       reduced=True,
                       surgery=True,
                       to_binary=True,
                       unify_surgery=True)
        except Exception as e:
            shutil.rmtree(paths['preprocessed'])
            return jsonify({ "error": f"Preprocessing failed: {e}" }), 500

    if not os.path.isdir(paths['classifications']):
        app.logger.info(f"Classifying export {export_id} for {model_category} with model {selected}")
        os.makedirs(paths['classifications'])

        try:
            classify(model_filename=paths['model'],
                     metadata_dir_path=paths['metadata'],
                     data_dir_path=paths['data'],
                     preprocessed_dir_path=paths['preprocessed'],
                     output_path=paths['classifications'],
                     codes=codes)
        except Exception as e:
            shutil.rmtree(paths['classifications'])
            return jsonify({ "error": f"Classification failed: {e}" }), 500

    classifications = []
    for filename in os.listdir(paths['classifications']):
        with open(os.path.join(paths['classifications'], filename)) as f:
            classification = json.load(f)
            classification['name'] = filename
            classifications.append(classification)

    return jsonify(classifications)
