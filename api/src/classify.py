import os

import pickle
import logging
import pandas as pd

from transform_to_json import transform_to_JSON
from preprocess import preprocess

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.DEBUG)
#logging.basicConfig(filename='preprocessing.log', filemode='a', format='%(asctime)s - %(message)s', level=logging.DEBUG)

def classify(data_dir_path, model_filename, preprocessed_dir_path, metadata_dir_path, output_path, codes):
    patient_filename = os.path.join(preprocessed_dir_path, 'bulk_classification_data.csv')

    if(model_filename[-7:] != '.pickle' ):
        model_filename = model_filename + '.pickle'
    try:
        with open(model_filename, 'rb') as handle:
            clf = pickle.load(handle)
    except(FileNotFoundError):
        logging.error('Could not find the model file %r.\n' % model_filename)
        exit()

    data = pd.read_csv(patient_filename, header=0, delimiter=",", quoting=0, encoding='utf8')
    for i in range(data.shape[0]):
        X = pd.DataFrame(columns = data.columns[:-1])
        X.loc[0] = data.loc[i][data.columns[:-1]]

        classdict = (clf.predict(X, prob=True))[0]

        file_path = os.path.join(output_path, X.loc[0]['participant_code'] + '.json')
        transform_to_JSON(clf, clf.feature_contribution(X), codes["admission_assessment"], out=file_path, diffsur=False, addline=classdict)

if __name__ == '__main__':
    codes = {
        # 'admission_assessment': 'QA',
        # 'follow_up_assessment': 'QF',
        # 'surgical_evaluation': 'QS'
        'admission_assessment': 'Q44071',
        'follow_up_assessment': 'Q92510',
        'surgical_evaluation': 'Q61802'
    }

    classify(data_dir_path='./EXPERIMENT_DOWNLOAD',
             model_filename='./models/prognostic_model_QF_lstPexMuscstrength[ElbowFlex]0',
             preprocessed_dir_path='./preprocessed_data',
             metadata_dir_path='./metadata',
             output_path='./classifications',
             codes=codes)
