import pandas as pd
import numpy as np
import re
import math
import csv
import os, fnmatch
import sys
import utils
import difflib
import pprint
import json
import glob
import logging
from datetime import datetime

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.DEBUG)
#logging.basicConfig(filename='preprocessing.log', filemode='a', format='%(asctime)s - %(message)s', level=logging.DEBUG)

CLASS_FIELDS = {'yonPexPain':['yonPexPain'],
'lstPexMuscstrength[ShoulderAbduc]': ['lstPexMuscstrengthL[LShoulderAbduc]', 'lstPexMuscstrengthR[RShoulderAbduc]'],
'lstPexMuscstrength[ElbowFlex]':['lstPexMuscstrengthR[RElbowFlex]', 'lstPexMuscstrengthL[LElbowFlex]'],
'lstPexMuscstrength[ShoulderExrot]':['lstPexMuscstrengthR[RShoulderExrot]', 'lstPexMuscstrengthL[LShoulderExrot]']}

OPERATION_TRAINING = 0
OPERATION_BULK_CLASSIFICATION = 1
OPERATION_SINGLE_CLASSIFICATION = 2

# some assumptions made in order for this script to work properly:
#   - the answers from the same patient should be together (no other patient answer between them)
#   - metadata filenames start with "Fields_[questionnaire_code]"

# the ideia is that I want to join columns that has the same meaning but that are separated for the sides (Right and Left).
# so I want to use the metadata info to do that.


def processMetadata(metadata):
    logging.info("Processing metadata")
    columns_info = metadata.loc[:, ['question_index', 'question_description', 'subquestion_description', 'question_scale', 'question_scale_label']].drop_duplicates()

    regex_sides = re.compile(r'Right|right|RIGHT|Left|left|LEFT')
    regex_right_side = re.compile(r'Right|right|RIGHT')
    regex_left_side = re.compile(r'Left|left|LEFT')

    descriptions = {}
    potential_right_codes = []
    potential_left_codes = []

    for code, description, sub_description, opt, side in columns_info.values:
        # Based on the column description, separate the side specific (RIGHT vs LEFT) columns
        if re.search(regex_right_side, description) or (not utils.isnan(side) and re.search(regex_right_side, str(side))):
            # Column potentially related to the right side
            potential_right_codes.append(code)
        elif re.search(regex_left_side, description) or (not utils.isnan(side) and re.search(regex_left_side, str(side))):
            # Column potentially related to the left side
            potential_left_codes.append(code)

        if not utils.isnan(sub_description) and sub_description != '':
            description = description + '[' + sub_description + ']'

        descriptions[code] = description

    codes_to_remove = []
    new_codes = []
    new_to_old_codes = {}

    # For each column potentially related to the Right side,
    # finds the correspondent column for the Left side and
    # create a new column to unify them. The code for the
    # new column will be the common parts of the left and right codes.

    for right_code in potential_right_codes:
        left_code = difflib.get_close_matches(right_code, potential_left_codes, n=1)[0]

        new_code = utils.str_intersection(right_code, left_code)
        new_code = re.sub(r'\[\]', "", new_code)
        new_codes.append(new_code)

        # Defines that the new column will be composed by the values from
        # the left and right columns
        new_to_old_codes[new_code] = [left_code, right_code]  #np.array([left_code, right_code])

        codes_to_remove.append(right_code)
        codes_to_remove.append(left_code)
        potential_left_codes.remove(left_code)

        try:
            descriptions[new_code] = re.sub(regex_sides, "Left/Right", descriptions[right_code])
        except(TypeError):
            descriptions[new_code] = descriptions[right_code]

    return new_codes, new_to_old_codes, codes_to_remove, descriptions


def unifyColumsBySide(data, new_field_names, new_to_old_names, class_questionnaire, classes, operation):
    logging.info('Unifying columns by side')

    final_data = {}
    for class_name in classes:
        side_code = data[class_name].filter(like='lstInjTpbiSide').columns[0]
        final_data[class_name] = pd.DataFrame(columns = new_field_names)

        j = 0
        for i in range(data[class_name].shape[0]):
            side = data[class_name][side_code][i]
            if side == 'RL':
                row_right = []
                row_left = []
                for field in new_field_names:
                    string = re.sub(r', |\n|;', '', str(np.array(data[class_name][new_to_old_names[field][-1]])[i]))
                    row_right.append(string)

                    string = re.sub(r', |\n|;', '', str(np.array(data[class_name][new_to_old_names[field][0]])[i]))
                    row_left.append(string)


                final_data[class_name].loc[j] = row_right
                final_data[class_name].loc[j]['participant_code'] += '-R'
                j = j+1
                final_data[class_name].loc[j] = row_left
                final_data[class_name].loc[j]['participant_code'] += '-L'

            elif side == 'R':
                row = []
                for field in new_field_names:
                    string = re.sub(r', |\n|;', '', str(np.array(data[class_name][new_to_old_names[field][-1]])[i]))
                    row.append(string)

                final_data[class_name].loc[j] = row

            else: #  side == 'L'  ou side == 'NINA'
                row = []
                for field in new_field_names:
                    string = re.sub(r', |\n|;', '', str(np.array(data[class_name][new_to_old_names[field][0]])[i]))
                    row.append(string)

                final_data[class_name].loc[j] = row

            j = j+1

        # drop every column where all values are NA
        final_data[class_name] = (final_data[class_name].T).dropna(how='all').T

        if(operation == OPERATION_TRAINING):
            class_code = class_questionnaire + '_' + class_name
            tmp = final_data[class_name][class_code]
            del final_data[class_name][class_code]
            final_data[class_name].insert(len(final_data[class_name].columns), class_code, tmp)

    return final_data


def differentiateNanFromNotApplicable(data, main_questionnaire, surgery_questionnaire=None):
    related_questions = {'yonPreFracture': data.filter(regex=r''+re.escape(main_questionnaire)+'.+PreFracture.*').columns,  #OK
    'yonPreOrtsurg': data.filter(regex=r''+re.escape(main_questionnaire)+'.+PreOrtsurg.*').columns, # OK, mas pega uma coluna a mais que a versao anterior, a lstPreOrtsurgClavicu (que nao era pega na versao em portugues pq estava escrita errada no questionario: opcLdClaviculaCorPr)
    'yonPrePain': data.filter(regex=r''+re.escape(main_questionnaire)+'.+PrePain.*').columns,  #OK
    'yonInjFracture': data.filter(regex=r''+re.escape(main_questionnaire)+'.+InjFracture.*').columns, #OK
    'yonInjOrtsurg': data.filter(regex=r''+re.escape(main_questionnaire)+'.+InjOrtsurg.*').columns, # NOK: lstImjOrtsurg + lstInjOrtsurg (tem um campo com erro de grafia: lstImjOrtsurgFace)
    'yonInjChesttube': data.filter(regex=r''+re.escape(main_questionnaire)+'.+InjChesttube.*').columns,  # OK
    'yonInjVascinj': data.filter(regex=r''+re.escape(main_questionnaire)+'.+InjVascinj.*').columns,    # NOK: InjVascinj + InjLSubclavianArt + InjRSubclavianArt + InjLBrachialArt + InjRBrachialArt + InjLAxillaryArt + InjRAxillaryArt
    'yonInjPhysio': data.filter(regex=r''+re.escape(main_questionnaire)+'.+InjPhysio.*').columns,      # NOK: pega uma coluna que na versao original nao pegava, a snDomiciliarAt -> yonInjPhysioHome
    'yonInjOrthesis': data.filter(regex=r''+re.escape(main_questionnaire)+'.+InjOrthesis.*').columns,  # NOK: InjOrthesis + InjLOrthesis + InjROrthesis -
    'yonInjMedicatio':data.filter(regex=r''+re.escape(main_questionnaire)+'.+InjMedicatio.*').columns, # OK
    'lstPexInspectio[Swelling]':data.filter(regex=r''+re.escape(main_questionnaire)+'.+PexSwelling.*').columns,   # OK, mas na versao original o filtro estava errado "Lcdema" (deveria ser LcEdema)
    'lstPexInspectio[SurgicalScar]': data.filter(regex=r''+re.escape(main_questionnaire)+'.+PexScar.*').columns,  # OK
    'lstPexInspectio[TrophicChanges]': data.filter(regex=r''+re.escape(main_questionnaire)+'.+PexTrophism.*').columns,  # OK
    'lstPexTinel': data.filter(regex=r''+re.escape(main_questionnaire)+'.+mulPexTinel.*').columns }    # OK

    for rq in related_questions.keys():
        columns = related_questions[rq]
        column_name = data.filter(like=rq).columns[0]

        for ix, row in data.iterrows():
            if(row[column_name] == 'N'):
                rq_i = 0
                while(rq_i < (len(columns))):
                    if(column_name in columns[rq_i]):
                        rq_i += 1
                        continue
                    #if(not utils.isnan(row[columns[rq_i]]) and row[columns[rq_i]] != 'NINA' and row[columns[rq_i]] != 'NAIA'):
                        ### set warning. if this happens, then the data is inconsistent
                    # if('opc' not in columns[rq_i]):
                    #    data.set_value(ix, columns[rq_i], 'N')#ão Aplicável')
                    # else:
                    data.at[ix, columns[rq_i]] = 'Not applicable'
                    rq_i+=1
    if(surgery_questionnaire):
        surgery_columns = data.filter(like=surgery_questionnaire).columns

        for ix, row in data.iterrows():
            if row[data.filter(like='yonInjBpsurg').columns[0]] != 'N':
                continue

            rq_i = 0
            while(rq_i < (len(surgery_columns))):
                if('equBpsurgTime' not in surgery_columns[rq_i]  and utils.isnan(row[surgery_columns[rq_i]])):
                    data.at[ix, surgery_columns[rq_i]] = 'Not applicable'
                rq_i+=1
    return data

def differentiatePreAndPostSurgery(data, class_name):
    questionnaires = []
    columns_to_change = ['lstPexInspectio', 'lstPexScoliosis', 'lstPexTinel', 'lstPexTouch', 'lstPexPain',
        'lstPexArthre', 'lstPexKine', 'lstPexPalle', 'yonInjPhysio', 'mulInjOrthesisType',
        'txtInjMedicatioList', 'yonPexPain', 'yonInjMedicatio', 'lstPexMuscstrength', ' intPex']

    for questionnaire in data.columns[data.columns.str.contains('_yonInjBpsurg')]:

        m = re.match('(Q\d+)\_', questionnaire)
        if m:
            q = m.group(1)
        for column in columns_to_change:
            for column_name in data.columns[data.columns.str.startswith(q+'_'+column)]:
                if(column_name == class_name):
                    continue
                for i in data.index:
                    if(data.loc[i, column_name] != 'NAIA' and
                        data.loc[i, column_name] != 'NINA' and not utils.isnan(data.loc[i, column_name])):
                            if(data[q+'_yonInjBpsurg'][i] == 'Y'):
                                data.loc[i, column_name] = data.loc[i, column_name] + ' post'
                            # here NINA's on yonInjBpsurg are considered "no"
                            else:
                                data.loc[i, column_name] = data.loc[i, column_name] + ' pre'

    return data


def to_scores(filename): ############# DUVIDA: onde sera usado? Pode ser removido?

    roots = ['C5', 'C6', 'C7', 'C8', 'T1', 'RC5', 'RC6', 'RC7', 'RC8', 'RT1', 'LC5', 'LC6', 'LC7', 'LC8', 'LT1']
    segment = ['Index', 'Elbow', 'Shoulder', 'RIndex', 'RElbow', 'RShoulder', 'LIndex', 'LElbow', 'LShoulder']
    segment2 = ['Clavicle', 'Elbow', 'Ulna', 'RClavicle', 'RElbow', 'RUlna', 'LClavicle', 'LElbow', 'LUlna']

    modalities = {
        'lstPexTouch': {'Abs':2, 'Hyper':1, 'Hypo':1, 'Norm':0, 'EvaluatedOn':roots},
        'lstPexPain': {'Abs':2, 'Hyper':1, 'Hypo':1, 'Norm':0, 'EvaluatedOn':roots},
        'lstPexArthre': {'NPv':1, 'Pv':0, 'EvaluatedOn':segment},
        'lstPexKine': {'NPv':1, 'Pv': 0, 'EvaluatedOn': segment},
        'lstPexPalle': {'Abs':2, 'Hypo':1, 'Pt':0, 'EvaluatedOn': segment2}}

    modality_score = 0
    root_score = 0

    data = pd.read_csv(filename, header=0, delimiter=",", quoting=0, encoding='utf8')
    for modality in modalities.keys():
        columns_names = data.columns[data.columns.str.contains(modality)]


#convert numeric class column values into two classes given a threshold,
#so that instances whose value <= threshold belong to class1 and whose
#value > threshold belong to class2
def numeric_to_binary(data, feature, class1, class2, threshold):#, out):
    for column in (data.filter(like=feature).columns):
        d = {'True':class1, True:class1, 'False':class2, False:class2, 'NINA':'NINA'}

        #True when class value <= threshold and False otherwise
        comp_threshold = lambda x: np.array([float(a) < threshold if (utils.isfloat(a) or utils.isint(a)) else 'NINA' for a in x])
        #class1 when value is True and class2 when it's False

        mask = [d[l] for l in comp_threshold(data[column])]
        data[column] = mask

    return data

def time_to_categorical(data, feature, categories, thresholds):
    if(len(thresholds) != len(categories)):
        logging.error('Categories size does not match thresholds size.')
        exit(-1)
    for ix, row in data.iterrows():
        for column in (data.filter(like=feature).columns):
            t = 0
            while t < len(thresholds):
                if(not utils.isnan(row[column]) and int(float(row[column])/30) <= thresholds[t]):
                    data.at[ix, column] = categories[t]
                    break
                else:
                    t+=1

    return data


# concat data files on participant code, follow-up being the one that we need to preserve all the rows, and
# the other ones being complementary. At this point the attributes need to be preceded by an id for the questionnaires.
# Then union metadata files adding the questionnaire id to the name of the attributes. Then produce the data with
# the output file -> that no longer will be a file.
def get_data(filename, condition):
    r = pd.read_csv(filename, header=0, delimiter=",", quoting=0, encoding='utf8')
    questionnaire_id = re.search('.*(QA|QS|QF)', filename).group(1)
    r.columns = [questionnaire_id + '_' + column for column in r.columns]
    r = r.rename(columns={questionnaire_id+'_'+condition: condition})

    r = r.sort_values(['participant_code']).reset_index(drop=True)

    return r

def get_metadata(filename, condition):
    r = pd.read_csv(filename, header=0, delimiter=",",
        quoting=0, encoding='utf8')
    questionnaire_id = re.search('.*(QA|QS|QF)', filename).group(1)

    r['question_code'] = [questionnaire_id + '_']*len(r['question_code']) + r['question_code']
    r.loc[r['question_code'] == questionnaire_id+'_'+condition, 'question_code'] = condition
    r['question_index'] = [questionnaire_id + '_']*len(r['question_index']) + r['question_index']
    r.loc[r['question_index'] == questionnaire_id+'_'+condition, 'question_index'] = condition

    for i in range(len(r['question_description'])):
        if(utils.isnan(r['question_description'][i])):
            r['question_description'].iloc[i] = str(r['question_code'][i])
        r['question_description'].loc[i] = str(questionnaire_id + '_' + r['question_description'][i])


    r.loc[r['question_description'] == questionnaire_id + '_' + condition, 'question_description'] = condition

    return r



#the file with the class inner join entrada if it's not entrada
#then left join the other ones
def join_data_files(list_of_files, condition, main_questionnaire, class_questionnaire, surgery_questionnaire, operation, classes, surgery=True, unify_surgery=True):
    logging.info('Loading data from questionnaires')

    cq = False
    for file_index in range(len(list_of_files)):
        if main_questionnaire in list_of_files[file_index]:
            tmp = list_of_files[0]
            list_of_files[0] = list_of_files[file_index]
            list_of_files[file_index] = tmp
        elif class_questionnaire in list_of_files[file_index]:
            cq = True
            if len(list_of_files) > 1:
                k = 1
            else:
                k = 0
            tmp = list_of_files[k]
            list_of_files[k] = list_of_files[file_index]
            list_of_files[file_index] = tmp

    d = treat_main_questionnaire_data(list_of_files[0], condition, main_questionnaire)

    data = {}
    k = 1
    if(cq):
        r_to_merge = treat_class_questionnaire_data(list_of_files[k], condition, class_questionnaire, classes, unify_surgery)
        # Since the processing made in treat_class_questionnaire_data depends on the chosen classes,
        # it returns a dict of dfs, one for each class

        if operation == OPERATION_TRAINING:
            for class_name in classes:
                data[class_name] = d.merge(r_to_merge[class_name], how = 'inner', on=condition)
        else: # for classification operation, the join do not need to be "inner"
              # since patient do not need to have all questionnaires fille in order to be classified
            for class_name in classes:
                data[class_name] = d.merge(r_to_merge[class_name], how = 'left', on=condition)

        k += 1
    else:
        for class_name in classes:
            data[class_name] = d

    if len(list_of_files) > k:
        s = None

        for file_index in range(k, len(list_of_files)):
            if(surgery_questionnaire in list_of_files[file_index]):
                if surgery:
                    s_to_merge = treat_surgical_questionnaire_data(list_of_files[file_index], condition, surgery_questionnaire)
                #else: ignore the file
            else:
                s_to_merge = get_data(list_of_files[file_index], condition)

            if(s is None):
                s = s_to_merge
            else:
                s = s.merge(s_to_merge, how = 'outer', on=condition)

        for class_name in classes:
            data[class_name] = data[class_name].merge(s, how = 'left', on = condition)

    if (operation == OPERATION_TRAINING and unify_surgery):
        for class_name in classes:
            for ix, row in data[class_name].iterrows():
                if(row[main_questionnaire+'_yonInjBpsurg'] != 'Y'):
                    if(row[class_questionnaire+'_yonInjBpsurg'] == 'Y' or not utils.isnan(row[surgery_questionnaire+'_equBpsurgTime'])):
                        data[class_name].at[ix, main_questionnaire+'_yonInjBpsurg'] = 'Y'

    return data

def treat_main_questionnaire_data(filename, condition, main_questionnaire_code):
    logging.info("Preprocessing questionnaire %s" % main_questionnaire_code)
    r = get_data(filename, condition)

    columns = np.array(r.columns)
    for column_index in range(len(columns)):
        if('[' in columns[column_index]):
            m = re.match('(.+)(\[.+\])', columns[column_index])
            if m:
                variable_name = m.group(1)
            else:
                logging.error('Regex not found: {0} '.format(columns[column_index]))

            variable_columns = r.filter(like=variable_name)
            variable_columns = variable_columns.filter(like=variable_name)#(regex=r''+re.escape(variable_name) + '(?!\[NINA\])')
            for ix, row in variable_columns.iterrows():
                for ir in range(len(row)):
                    if(not utils.isnan(row[ir])):
                        for it in range(len(row)):
                            if(it == ir):
                                continue
                            else:
                                if(utils.isnan(r[variable_columns.columns[it]][ix])):
                                    if((variable_name+'[NINA]' not in r.columns and variable_name + '[NAIA]' not in r.columns) or
                                        ((variable_name+'[NINA]' in r.columns and r[variable_name+'[NINA]'][ix] != 'Y') or
                                          (variable_name+'[NAIA]' in r.columns and r[variable_name+'[NAIA]'][ix] != 'Y'))):
                                        r.ix[ix, variable_columns.columns[it]] = 'N'


    r = r.drop((r.filter(like='[NINA]').columns), axis=1)
    for ix, row in r.filter(like=main_questionnaire_code+'\_mulInjTbpiCause[other]'):
        if(not utils.isnan(row)):
            r.at[ix, main_questionnaire_code+'_mulInjTbpiCause[other]'] = 'Y'

    logging.debug('Size of %s: %s' %(main_questionnaire_code, r.shape))

    return r

def class_value_is_valid(row, class_questionnaire, class_name):

    for field in CLASS_FIELDS[class_name]:
        field = class_questionnaire+'_'+field
        if row[field] != 'NAIA' and row[field] != 'NINA' and not utils.isnan(row[field]) :
            return True

    return False


def treat_class_questionnaire_data(filename, condition, class_questionnaire, classes, unify_surgery):
    logging.info("Preprocessing questionnaire %s" % class_questionnaire)
    tmp = get_data(filename, condition)

    acquisition_time_code = tmp.filter(like=class_questionnaire+'_equInjTbpiTime').columns[0]

    r_to_merge = {} # dict of dataframes, one for each class
    for class_name in classes:
        r_to_merge[class_name] = pd.DataFrame(columns = tmp.columns)

    i = 0
    for ix, row in tmp.iterrows():
        for class_name in classes:
            if i == 0 or tmp[condition][i] != r_to_merge[class_name][condition].values[-1]:
                r_to_merge[class_name].loc[i] = row
            else:
                if(tmp[acquisition_time_code][ix] > r_to_merge[class_name][acquisition_time_code].values[-1] and
                    (class_name == '' or class_value_is_valid(row, class_questionnaire, class_name))):
                    r_to_merge[class_name].iloc[-1] = row
                elif (unify_surgery and row[class_questionnaire+'_yonInjBpsurg'] == 'Y'):
                    r_to_merge[class_name].loc[r_to_merge[class_name].tail(1).index, class_questionnaire+'_yonInjBpsurg'] = row[class_questionnaire+'_yonInjBpsurg']
        i += 1

    for class_name in classes:
        logging.debug('Size of %s for class %s: %s' %(class_questionnaire, class_name, r_to_merge[class_name].shape))

    return r_to_merge


def treat_surgical_questionnaire_data(filename, condition, surgical_questionnaire_code):
# Explicações extraídas da dissertação da Luciana:
# Do questionário de Avaliação Cirúrgica, foram consideradas apenas as questões
# que indicam o tipo de procedimento realizado, o sítio do procedimento (quais nervos, raízes, etc.)
# e o intervalo de tempo entre a lesão e o primeiro procedimento cirúrgico de cada paciente.
# É necessário unificar os
# preenchimentos do questionário de Avaliação Cirúrgica de um mesmo paciente. Isso pode ser feito
# unindo as respostas positivas de cada preenchimento do paciente para as questões relativas ao tipo
# e ao sítio de cada procedimento realizado. Em relação ao intervalo de tempo entre a lesão e a cirur-
# gia, pode-se considerar o intervalo do primeiro procedimento cirúrgico realizado.

    logging.info("Preprocessing questionnaire %s" % surgical_questionnaire_code)
    tmp = get_data(filename, condition)

    acquisition_time_code = tmp.filter(like=surgical_questionnaire_code+'_equBpsurgTime').columns[0]
    remaining_columns = ['participant_code', 'equBpsurgTime', 'lstBpsurgSide',
    'mulBpsurgeryType', 'mulLysisSite[', 'mulLysisRoot', 'mulLysisNerve', 'mulLysisTrunk',
    'mulLysisDivision', 'mulLysisCord', 'mulTransfSite', 'mulGraftLevel[', 'mulGraftRoot', 'mulGraftTrunk',
    'mulGraftNerve', 'mulNeuromadSite']
    code_procedure = {'Lysis':'[Lysis]' , 'Transf':'[Transf]', 'Graft':'[Graft]', 'Neuromad':'[Neuromad]', 'nan':'[NINA]'}
    nan_codes = {'mulBpsurgeryType[NINA]': '(mulBpsurgeryType)(?!\[NINA\])',
        'mulLysisSite[NINA]': '(mulLysisSite)(?!\[NINA\])',
        'mulLysisNerve[NINA]':'(mulLysisNerve)(?!\[NINA\])',
        'mulLysisTrunk[NINA]':'(mulLysisTrunk)(?!\[NINA\])',
        'mulLysisCord[NINA]':'(mulLysisCord)(?!\[NINA\])',
        'mulLysisCord[NINA]':'(mulLysisCord)(?!\[NINA\])',
        'mulLysisDivision[NINA]': '(mulLysisDivision)(?!\[NINA\])',
        'mulTransfSite[NINA]':'(mulTransfSite)(?!\[NINA\])',
        'mulGraftLevel[NINA]':'(mulGraftLevel)(?!\[NINA\])',
        'mulGraftRoot[NINA]':'(mulGraftRoot)(?!\[NINA\])',
        'mulGraftTrunk[NINA]':'(mulGraftTrunk)(?!\[NINA\])',
        'mulGraftNerve[NINA]':'(mulGraftNerve)(?!\[NINA\])',
        'mulNeuromadSite[NINA]':'(mulNeuromadSite)(?!\[NINA\])'}
    df = tmp.filter(like=remaining_columns[0])
    for rc_index in range(1, len(remaining_columns)):
        df = df.join(tmp.filter(like=remaining_columns[rc_index]))

    r_to_merge = pd.DataFrame(columns=df.columns, dtype=str)

    for i, row in df.iterrows():

        if i == 0 or row[condition] != r_to_merge[condition].values[-1]:
            # Insert as new record in the dataframe
            r_to_merge.loc[r_to_merge.shape[0]] = row.values

            for procedure in code_procedure.keys():

                # Fill with 'N' the surgery procedures that the patient have not made
                if(r_to_merge[surgical_questionnaire_code+'_mulBpsurgeryType'+code_procedure[procedure]].values[-1] == 'Y'):
                    css = r_to_merge.filter(regex=r'(mulBpsurgeryType)(?!' + re.escape(code_procedure[procedure])+ ')').columns
                    for j in range(len(css)):
                        if(r_to_merge[css[j]].values[-1] != 'Y'):
                            r_to_merge.ix[r_to_merge.shape[0]-1, css[j]] = 'N'

                for rc in remaining_columns[3:]:
                    if(procedure in rc):
                        css = r_to_merge.filter(like=rc).columns
                        for cs in css:
                            if(r_to_merge[cs].values[-1] != 'Y' and
                                r_to_merge[surgical_questionnaire_code+'_mulBpsurgeryType'+code_procedure['nan']].values[-1] != 'Y'):
                                r_to_merge.ix[r_to_merge.shape[0]-1, cs] = 'N'

            for nan_code in nan_codes.keys():
                if(r_to_merge[surgical_questionnaire_code+'_'+nan_code].values[-1] == 'Y'):
                    r_to_merge.at[r_to_merge.index[-1], r_to_merge.filter(regex=r''+nan_codes[nan_code]).columns] = np.nan


        else:

            if row[acquisition_time_code] < r_to_merge[acquisition_time_code].values[-1]:
                r_to_merge.at[r_to_merge.index[-1], acquisition_time_code] =  row[acquisition_time_code]
            if(row[surgical_questionnaire_code+'_mulBpsurgeryType'+code_procedure['nan']] == 'Y'):
                continue

            for procedure in code_procedure.keys():
                if(row[surgical_questionnaire_code+'_mulBpsurgeryType'+code_procedure[procedure]] == 'Y'):
                    r_to_merge.at[r_to_merge.index[-1], surgical_questionnaire_code+'_mulBpsurgeryType'+code_procedure[procedure]] = 'Y'
                    css = r_to_merge.filter(regex=r'(mulBpsurgeryType)(?!' + re.escape(code_procedure[procedure])+ ')').columns
                    for cs in css:
                        if(r_to_merge[cs].values[-1] != 'Y'):
                            r_to_merge.ix[r_to_merge.shape[0]-1, cs] = 'N'
                for rc in remaining_columns[3:]:
                    if(procedure in rc):
                        css = r_to_merge.filter(like=rc).columns
                        for cs in css:
                            if(row[cs] == 'Y'):
                                r_to_merge.ix[r_to_merge.shape[0]-1, cs] = 'Y'
                            else:
                                if(r_to_merge[cs].values[-1] != 'Y'):
                                    r_to_merge.ix[r_to_merge.shape[0]-1, cs] = 'N'

    nan_codes_with_questionnaire_id = np.array(list(nan_codes.keys()), dtype=object)

    for i in range(len(nan_codes_with_questionnaire_id)):
        nan_codes_with_questionnaire_id[i] = surgical_questionnaire_code+ '_'+ nan_codes_with_questionnaire_id[i]
    r_to_merge = r_to_merge.drop(nan_codes_with_questionnaire_id, axis=1)

    logging.debug('Size of %s: %s' %(surgical_questionnaire_code, r_to_merge.shape))

    return r_to_merge


def get_frequencies_of_return(filename, condition):  ######### DUVIDA: onde sera usado? Pode ser removido?

    data = pd.read_csv(filename, header=0, delimiter=",", quoting=0, encoding='utf8')
    dateformat = '%Y-%m-%d %H:%M:%S'

    e_acquisitiondate_code = data.filter(like=admission_assessment_code+'_acquisitiondate').columns[0]
    s_acquisitiondate_code = data.filter(like=follow_up_assessment_code+'_acquisitiondate').columns[0]
    periods = {}

    for i in range(data.shape[0]):
        d = (datetime.strptime(data[s_acquisitiondate_code][i], dateformat) - datetime.strptime(data[e_acquisitiondate_code][i], dateformat))
        d = round(d.days/30)
        if d not in periods.keys():
            periods[d] = 1
        else:
            periods[d] += 1

    import matplotlib.pyplot as plt
    k = sorted(periods.items(), key=lambda x: x[0])
    plt.bar(range(0, 2*len([i[0] for i in k]), 2), [i[1] for i in k])
    pos = np.arange(0, 2*len(k), 2)
    width = 1.0
    ax = plt.axes()
    ax.set_xticks(pos + (width / 2))
    ax.set_xticklabels([i[0] for i in k])
    plt.xlabel('Time interval (months)')
    plt.ylabel('Frequency')
    plt.show()


def join_metadata_files(list_of_files, condition, surgical_evaluation_code):
    filename = list_of_files[0]
    r = get_metadata(filename, condition)

    for file_index in range(1, len(list_of_files)):
        if(surgical_evaluation_code not in list_of_files[file_index]):
            r_to_merge = get_metadata(list_of_files[file_index], condition)
            r = r.append(r_to_merge)

    return r

def reduce(data, main_questionnaire, class_questionnaire, surgery_questionnaire, class_name, condition):
    r_columns = [condition, main_questionnaire+'_yonPreFracture',  main_questionnaire+'_yonPreOrtsurg',
    main_questionnaire+'_yonPreBrainsurg', main_questionnaire+'_yonPreNersurg', main_questionnaire+'_yonPreTbi', main_questionnaire+'_yonPreTsci',
    main_questionnaire+'_yonPrePain', main_questionnaire+'_equInjTbpiAge', main_questionnaire+'_lstInjTpbiSide',
    main_questionnaire+'_mulInjTbpiCause[Motorcycle]', main_questionnaire+'_yonInjFracture', main_questionnaire+'_yonInjDisloc',
    main_questionnaire+'_yonInjTbi', main_questionnaire+'_yonInjOrtsurg', main_questionnaire+'_yonInjBrainsurg', main_questionnaire+'_yonInjTsci',
    main_questionnaire+'_yonInjChesttube', main_questionnaire+'_yonInjVascinj', main_questionnaire+'_yonInjUnconscio',
    main_questionnaire+'_yonInjPhysio', main_questionnaire+'_mulInjOrthesisType[ArmSling]', main_questionnaire+'_txtInjMedicatioList[Opioids_Name]',
    main_questionnaire+'_txtInjMedicatioList[Antidepressants_Name]', main_questionnaire+'_txtInjMedicatioList[Anticonvulsants_Name]',
    main_questionnaire+'_txtInjMedicatioList[Neuroleptics_Name]', main_questionnaire+'_yonInjBpsurg', main_questionnaire+'_lstInjPainsurg',
    main_questionnaire+'_lstPexInspectio[ShoulderSubdisloc]', main_questionnaire+'_lstPexInspectio[ScapulaAlata]',
    main_questionnaire+'_lstPexInspectio[HornerSyndrome]', main_questionnaire+'_lstPexInspectio[Swelling]',
    main_questionnaire+'_lstPexInspectio[SurgicalScar]', main_questionnaire+'_lstPexInspectio[TrophicChanges]',
    main_questionnaire+'_lstPexScoliosis[Scoliosis]', main_questionnaire+'_lstPexTinel[TinelSign]', main_questionnaire+'_lstPexTouch[C5]',
    main_questionnaire+'_lstPexTouch[C6]', main_questionnaire+'_lstPexTouch[C7]', main_questionnaire+'_lstPexTouch[C8]',
    main_questionnaire+'_lstPexTouch[T1]', main_questionnaire+'_lstPexPain[C5]', main_questionnaire+'_lstPexPain[C6]',
    main_questionnaire+'_lstPexPain[C7]', main_questionnaire+'_lstPexPain[C8]', main_questionnaire+'_lstPexPain[T1]',
    main_questionnaire+'_lstPexArthre[Index]', main_questionnaire+'_lstPexArthre[Shoulder]',
    main_questionnaire+'_lstPexArthre[Elbow]', main_questionnaire+'_lstPexKine[Index]',
    main_questionnaire+'_lstPexKine[Elbow]', main_questionnaire+'_lstPexKine[Shoulder]',
    main_questionnaire+'_lstPexPalle[Clavicle]',  main_questionnaire+'_lstPexPalle[Elbow]',
    main_questionnaire+'_lstPexPalle[Ulna]', main_questionnaire+'_intPexShoulderFlex', main_questionnaire+'_intPexShoulderAbdu',
    main_questionnaire+'_intPexShoulderExrot', main_questionnaire+'_intPexElbowFlex', main_questionnaire+'_intPexElbowExte',
    main_questionnaire+'_intPexForearmSupin', main_questionnaire+'_intPexForearmProna', main_questionnaire+'_intPexWristFlex',
    main_questionnaire+'_intPexWristExte', main_questionnaire+'_lstPexMuscstrength[ShoulderAbduc]', main_questionnaire+'_lstPexMuscstrength[ShoulderExrot]',
    main_questionnaire+'_lstPexMuscstrength[ShoulderInrot]', main_questionnaire+'_lstPexMuscstrength[ScapulaElev]',
    main_questionnaire+'_lstPexMuscstrength[ScapulaAbducSuprot]', main_questionnaire+'_lstPexMuscstrength[ElbowFlex]',
    main_questionnaire+'_lstPexMuscstrength[ElbowExte]', main_questionnaire+'_lstPexMuscstrength[WristExte]',
    main_questionnaire+'_lstPexMuscstrength[WristFlex]', main_questionnaire+'_lstPexMuscstrength[FingersFlex]',
    main_questionnaire+'_lstPexMuscstrength[FingersAbdu]', main_questionnaire+'_lstPexMuscstrength[FingersAddu]',
    main_questionnaire+'_lstPexMuscstrength[ThumbOppo]', main_questionnaire+'_yonPexPain',
    main_questionnaire+'_mulInjOrthesisType[ShoulderSupport]', main_questionnaire+'_equInjTbpiTime']

    if(class_questionnaire):
        r_columns.append(class_questionnaire+'_equInjTbpiTime')

    surgery_columns = data.filter(like=surgery_questionnaire).columns
    remaining_columns = r_columns + list(surgery_columns)
    if(class_questionnaire):
        remaining_columns = remaining_columns + [class_questionnaire+'_'+class_name]
    data = data[remaining_columns]
    medic_columns = data.filter(like=main_questionnaire+'_txtInjMedicatioList').columns
    for ix, row in data[medic_columns].iterrows():
        for j in range(len(medic_columns)):
            if(row[medic_columns[j]] != 'N' and not utils.isnan(row[medic_columns[j]])):
                data.at[ix, medic_columns[0]] = 'Y'
    data = data.rename(columns = {medic_columns[0]:main_questionnaire+'_txtInjMedicatioListNer'})
    data = data.drop(medic_columns[1:], axis=1)

    sens_columns = list(data.filter(like=main_questionnaire+'_lstPexTouch').columns)
    sens_columns = sens_columns + list(data.filter(like=main_questionnaire+'_lstPexPain').columns)

    for ix, row in data[sens_columns].iterrows():
        for j in range(len(sens_columns)):
            if(row[sens_columns[j]] != 'Norm' and not utils.isnan(row[sens_columns[j]])):
                data.at[ix, sens_columns[j]] = 'NPv'

    return data

def transformNumericFeaturestoBinary(data, main_questionnaire):
    data = time_to_categorical(data, '_equInjPhysioTime', ['0 to 6 months', '7 to 12 months', '13 to 24 months', '25 months or more'], [6, 12, 24, float('inf')])
    data = time_to_categorical(data, '_equInjTbpiTime', ['0 to 6 months', '7 to 12 months', '13 to 24 months', '25 months or more'], [6, 12, 24, float('inf')])
    data = time_to_categorical(data, '_equBpsurgTime', ['0 to 6 months', '7 to 12 months', '13 to 24 months', '25 months or more'], [6, 12, 24, float('inf')])
    data = numeric_to_binary(data, main_questionnaire+'_lstPexMuscstrength', 'Less than 3', 'Greater than or equals to 3', 3)
    data = numeric_to_binary(data, main_questionnaire+'_equInjTbpiAge', 'Less than 30', 'Greater than or equals to 30', 30)
    data = numeric_to_binary(data, main_questionnaire+'_intPexShoulderFlex', 'Less than 180', 'Greater than or equals to 180', 180)
    data = numeric_to_binary(data, main_questionnaire+'_intPexShoulderExte', 'Less than 50', 'Greater than or equals to 50', 50)
    data = numeric_to_binary(data, main_questionnaire+'_intPexShoulderAbdu', 'Less than 170', 'Greater than or equals to 170', 170)
    data = numeric_to_binary(data, main_questionnaire+'_intPexShoulderExrot', 'Less than 60', 'Greater than or equals to 60', 60)
    data = numeric_to_binary(data, main_questionnaire+'_intPexElbowFlex', 'Less than 40', 'Greater than or equals to 40', 40)
    data = numeric_to_binary(data, main_questionnaire+'_intPexElbowExte', 'Less than 180', 'Greater than or equals to 180', 180)
    data = numeric_to_binary(data, main_questionnaire+'_intPexForearmSupin', 'Less than 80', 'Greater than or equals to 80', 80)
    data = numeric_to_binary(data, main_questionnaire+'_intPexForearmProna', 'Less than 80', 'Greater than or equals to 80', 80)
    data = numeric_to_binary(data, main_questionnaire+'_intPexWristFlex', 'Less than 60', 'Greater than or equals to 60', 60)
    data = numeric_to_binary(data, main_questionnaire+'_intPexWristExte', 'Less than 60', 'Greater than or equals to 60', 60)

    return data

def load_metadata(path, metadata_path, field_names, surgical_evaluation_code, language):

    unified_fields_file_path = os.path.join(metadata_path, 'metadata_unified_fields.json')
    removed_fields_file_path = os.path.join(metadata_path, 'metadata_removed_fields_mappings.json')
    field_mappings_file_path = os.path.join(metadata_path, 'metadata_field_mappings.json')
    field_descriptions_file_path = os.path.join(metadata_path, 'metadata_field_descriptions.json')

    if os.path.isfile(unified_fields_file_path) and os.path.isfile(removed_fields_file_path) and os.path.isfile(field_mappings_file_path) and os.path.isfile(field_descriptions_file_path):
        logging.info("Loading saved processed metadata")

        with open(unified_fields_file_path) as json_file:
            unified_field_names = json.load(json_file)

        with open(removed_fields_file_path) as json_file:
            fields_to_remove = json.load(json_file)

        with open(field_mappings_file_path) as json_file:
            new_to_old_names = json.load(json_file)

#        fields_to_remove = []
#        for field in unified_field_names:
#            fields_to_remove += new_to_old_names[field]

        #with open(field_descriptions_file_path) as json_file:
        #    field_descriptions = json.load(json_file)

    else:
        # There is no processed metadata saved to recover,
        # so questionnaire metadata must be processed
        metadata_paths = utils.findFiles('**/Questionnaire_metadata/**/Fields*'+language+'.csv', path)

        if len(metadata_paths) == 0:
            logging.error("Questionnaires' metadata not found")
            raise Exception("Questionnaires' metadata not found")

        metadata = join_metadata_files(metadata_paths, 'participant_code', surgical_evaluation_code)
        unified_field_names, new_to_old_names, fields_to_remove, field_descriptions = processMetadata(metadata)

        logging.info("Saving processed metadata")
        # save the processed metadata
        with open(unified_fields_file_path, 'w') as file:
            json.dump(unified_field_names, file)

        with open(removed_fields_file_path, 'w') as file:
            json.dump(fields_to_remove, file)

        with open(field_mappings_file_path, 'w') as file:
            json.dump(new_to_old_names, file)

        with open(field_descriptions_file_path, 'w') as file:
            json.dump(field_descriptions, file)

    new_field_names = []
    for field in unified_field_names:
        if new_to_old_names[field][0] in field_names:
            # Only new (unified) fields that are built on fields that exist
            # in the joined data will be kept
            new_field_names.append(field)

    for field in field_names:
        if field not in fields_to_remove:
            # The columns that were not marked to be "unified"
            # will be kept unchanged
            new_field_names.append(field)
            new_to_old_names[field] = [field]

    return new_field_names, new_to_old_names




def showShape(data, classes):
    s = ''
    for class_name in classes:
        s += class_name + ' ' + str(data[class_name].shape)
    return s

def saveData(data, classes):
    for class_name in classes:
        data[class_name].to_csv(class_name + '_data.csv', index=False)


def loadData(classes):
    data = {}
    for class_name in classes:
        data[class_name] = pd.read_csv(class_name + '_data.csv', header=0, delimiter=",", quoting=0, encoding='utf8')
    return data


def preprocess(path, output_path, main_questionnaire, class_questionnaire, surgery_questionnaire, classes=[''], metadata_path='', operation=OPERATION_TRAINING, participant_code=None, surgery=True, reduced=False, to_binary=True, not_applicable=False, unify_surgery=True, language='en'):
    logging.info('Starting new preprocessing for classes ' + str(classes))

    if len(classes) == 0:
        classes = ['']

    if metadata_path == '':
        metadata_path = output_path

    if operation == OPERATION_SINGLE_CLASSIFICATION:
        # get data_paths of per participant data
        data_paths = utils.findFiles('**/Per_participant*/Participant_'+participant_code+'/**/*.csv', path)
    else: # operation == OPERATION_TRAINING or operation == OPERATION_BULK_CLASSIFICATION
        # get data_paths of per questionnaire data
        data_paths = utils.findFiles('**/Per_questionnaire*/**/*.csv', path)

    if len(data_paths) == 0:
        logging.error("Questionnaires' data not found")
        raise Exception("Questionnaires' data not found")

    ######################################################################
    data = join_data_files(data_paths, 'participant_code', main_questionnaire, class_questionnaire, surgery_questionnaire, operation, classes, surgery, unify_surgery)
    #saveData(data, classes)
    #data = loadData(classes)
    ######################################################################
    logging.debug('Joint data size: {0}'.format(showShape(data, classes)))

    field_names = data[classes[0]].columns
    new_field_names, new_to_old_names = load_metadata(path, metadata_path, field_names, surgery_questionnaire, language)

    data = unifyColumsBySide(data, new_field_names, new_to_old_names, class_questionnaire, classes, operation)

    logging.debug('Unified data size: {0}'.format(showShape(data, classes)))

    for class_name in classes:

        if(operation == OPERATION_TRAINING):
            if class_name != 'yonPexPain':
                data[class_name] = numeric_to_binary(data[class_name], class_questionnaire+'_'+class_name, 'UNSATISFACTORY', 'SUCCESS', 3)
            else:
                for ix, row in data[class_name].iterrows():
                    if(row[class_questionnaire+'_'+class_name] == 'N'):
                        data[class_name].at[ix, class_questionnaire+'_'+class_name] = 'SUCCESS'
                    elif(row[class_questionnaire +'_'+class_name] == 'Y'):
                        data[class_name].at[ix, class_questionnaire+'_'+class_name] = 'UNSATISFACTORY'

        if(to_binary):
            logging.info('Transforming numeric features to binary for class {0}'.format(class_name))
            data[class_name] = transformNumericFeaturestoBinary(data[class_name], main_questionnaire)

        if(not_applicable):
            data[class_name] = differentiateNanFromNotApplicable(data[class_name], main_questionnaire=main_questionnaire, surgery_questionnaire=surgery_questionnaire)

        # if(dif_surgery):
        #     print('Adding pre and post surgery info to features...')
        #     data = differentiatePreAndPostSurgery(data[class_name], class_questionnaire+'_'+class_name)


        if(operation == OPERATION_TRAINING):
            data[class_name] = data[class_name].dropna(subset=[class_questionnaire+'_'+class_name])
            data[class_name] = data[class_name].drop(np.where([e == 'NAIA' or e == 'NINA' for e in data[class_name][data[class_name].columns[-1]]])[0])

        data[class_name] = data[class_name].drop(data[class_name].columns[data[class_name].columns.str.endswith('id')], 1)
        data[class_name] = data[class_name].drop(data[class_name].columns[data[class_name].columns.str.endswith('token')], 1)
        data[class_name] = (data[class_name].drop(data[class_name].columns[data[class_name].columns.str.endswith('ipaddr')], 1))
        data[class_name] = (data[class_name].drop(data[class_name].columns[data[class_name].columns.str.endswith('stamp')], 1))

        data[class_name] = (data[class_name].drop(data[class_name].columns[data[class_name].columns.str.endswith('gender')][1:], 1))
        if(reduced):
            if(operation == OPERATION_TRAINING):
                data[class_name] = reduce(data[class_name], main_questionnaire, class_questionnaire, surgery_questionnaire, class_name, 'participant_code')
            else:
                data[class_name] = reduce(data[class_name], main_questionnaire, False, surgery_questionnaire, class_name, 'participant_code')

        logging.debug('Reduced data size for class {0}: {1}'.format(class_name, data[class_name].shape))

        data_file_path = class_name+'_data.csv'
        if(operation == OPERATION_TRAINING):
            # Remove columns which has only nan values
            data[class_name] = data[class_name].drop(data[class_name].T[np.array([np.all([data[class_name][k] == 'nan']) for k in data[class_name]])].T.columns, axis=1)
        elif operation == OPERATION_BULK_CLASSIFICATION:
            data_file_path = 'bulk_classification' + data_file_path
        else: #operation == OPERATION_SINGLE_CLASSIFICATION
            data_file_path = participant_code + '_classification' + data_file_path

        data_file_path = os.path.join(output_path, data_file_path)

        data[class_name].to_csv(data_file_path, index=False)
        logging.debug('Final data size for class {0}: {1}'.format(class_name, data[class_name].shape))

    logging.info('Preprocessing finished')


def display_menu():
    valid = False
    while(not valid):
        dirname = input("Please provide the path for the directory containing questionnaire files (e.g. ~/Downloads/download/EXPERIMENT_DOWNLOAD):\n")
        if(not os.path.isdir(dirname)):
            print("%s is not a valid directory.\n" % dirname)
        else:
            output_dirname = input("Please provide the path for the directory where output files will be saved:\n")
            if(not os.path.isdir(output_dirname)):
                print("%s is not a valid directory.\n" % output_dirname)
            else:
                while(not valid):
                    ct = input('Enter "c" to preprocess file for classifying or "t" for training:\n')
                    if(ct[0] != 'c' and ct[0] != 't'):
                        print("%s is not a valid option.\n" % ct[0])
                    else:
    #                    output = input("Please provide output name for preprocessed file (default='out.csv'):\n")
    #                    if(len(output) > 0):
    #                        if(len(output) < 4 or output[-4:] != ".csv"):
    #                            output=output+".csv"
    #                    else:
    #                        output="out.csv"
                        valid = True
    if(ct == 'c'):
        participant_code = input('Please provide patient code (e.g.: P10666):\n')
        preprocess(dirname, output_dirname, admission_assessment_code, follow_up_assessment_code, surgical_evaluation_code, operation=OPERATION_SINGLE_CLASSIFICATION, participant_code=participant_code, not_applicable=True, reduced=True, surgery=True, to_binary=True, unify_surgery=True)
    else:
        class_name = input('Please provide class name (e.g.: yonPexPain, lstPexMuscstrength[ShoulderExrot], lstPexMuscstrength[ElbowFlex], lstPexMuscstrength[ShoulderAbduc]): ')
        preprocess(dirname, output_dirname, admission_assessment_code, follow_up_assessment_code, surgical_evaluation_code, [class_name], operation=OPERATION_TRAINING, not_applicable=True, reduced=True, surgery=True, to_binary=True, unify_surgery=True)

if __name__ == '__main__':
    #display_menu()
    #exit()

    admission_assessment_code = 'QA' #'Q44071'
    surgical_evaluation_code = 'QS' #'Q61802'
    follow_up_assessment_code = 'QF' #'Q92510'

    # Prepare data for model trainning
    #classes = ['yonPexPain']
    #classes = ['yonPexPain', 'lstPexMuscstrength[ShoulderExrot]', 'lstPexMuscstrength[ElbowFlex]', 'lstPexMuscstrength[ShoulderAbduc]']
    #preprocess('EXPERIMENT_DOWNLOAD', 'preprocessed_data', admission_assessment_code, follow_up_assessment_code, surgical_evaluation_code, classes, metadata_path='metadata', operation=OPERATION_TRAINING, not_applicable=True, reduced=True, surgery=True, to_binary=True, unify_surgery=True)

    # Prepare data for single classification
    # logging.info("Patient P2386 - has only admission assessment data")
    # preprocess('EXPERIMENT_DOWNLOAD', 'preprocessed_data', admission_assessment_code, follow_up_assessment_code, surgical_evaluation_code, operation=OPERATION_SINGLE_CLASSIFICATION, participant_code='P2386', not_applicable=True, reduced=True, surgery=True, to_binary=True, unify_surgery=True)
    # logging.info("Patient P10666 - has admission assessment and surgical evaluation data")
    # preprocess('EXPERIMENT_DOWNLOAD', 'preprocessed_data', admission_assessment_code, follow_up_assessment_code, surgical_evaluation_code, operation=OPERATION_SINGLE_CLASSIFICATION, participant_code='P10666', not_applicable=True, reduced=True, surgery=True, to_binary=True, unify_surgery=True)
    # logging.info("Patient P7408 - has data from the three questionnaires")
    # preprocess('EXPERIMENT_DOWNLOAD', 'preprocessed_data', admission_assessment_code, follow_up_assessment_code, surgical_evaluation_code, operation=OPERATION_SINGLE_CLASSIFICATION, participant_code='P7408', not_applicable=True, reduced=True, surgery=True, to_binary=True, unify_surgery=True)

    # Prepare data for bulk classification
    preprocess('EXPERIMENT_DOWNLOAD', 'preprocessed_data', admission_assessment_code, follow_up_assessment_code, surgical_evaluation_code, operation=OPERATION_BULK_CLASSIFICATION, not_applicable=True, reduced=True, surgery=True, to_binary=True, unify_surgery=True)
