import json
import pickle

def transform_to_JSON(clf, fcs, main_questionnaire_code, out='FeatureContributions.json', diffsur=True, X=None, addline=None):
    import json
    import pandas as pd
    import utils

    if (X is None):
        if (not isinstance(clf.X, pd.DataFrame)):
            X = pd.DataFrame(clf.X, columns=clf.attributes)
        else:
            X = clf.X

    #data = read.readData(data_path = data_path, class_name = class_name)
    #newcolumns = np.append(X.columns, ['Q44071_snCplexoAt', class_name])
    #newX = pd.merge(data, X, how='inner', on='Q44071_participant_code')[newcolumns]
    F = {}

    for i in range(len(fcs)):
        if (diffsur):
            for feature_index in fcs[i].keys():
                feature_name = clf.attributes[feature_index]

                if (feature_name not in F):
                    F[feature_name] = {
                        'y_categories' : sorted(list([a for a in set(X[X.columns[feature_index]]) if not utils.isnan(a)])) + ['nan'],
                        'red_o_points' : [],
                        'red_x_points' : [],
                        'blue_o_points' : [],
                        'blue_x_points' : []
                    }

                if (clf.X[main_questionnaire_code+'_yonInjBpsurg'][i] == 'S'):
                    if (clf.y[i] == 'UNSATISFACTORY'):
                        if (not utils.isnan(X[X.columns[feature_index]][i])):
                            F[feature_name]['red_o_points'].append([
                                round(fcs[i][feature_index], 5),
                                F[feature_name]['y_categories'].index(
                                    X[X.columns[feature_index]][i])
                            ])
                        else:
                            F[feature_name]['red_o_points'].append([
                                round(fcs[i][feature_index], 5),
                                len(F[feature_name]['y_categories'])-1
                            ])
                    else:
                        if (not utils.isnan(X[X.columns[feature_index]][i])):
                            F[feature_name]['blue_o_points'].append([
                                round(fcs[i][feature_index], 5),
                                F[feature_name]['y_categories'].index(
                                    X[X.columns[feature_index]][i])
                            ])
                        else:
                            F[feature_name]['blue_o_points'].append([
                                round(fcs[i][feature_index], 5),
                                len(F[feature_name]['y_categories'])-1
                            ])
                else:
                    if (clf.y[i] == 'UNSATISFACTORY'):
                        if (not utils.isnan(X[X.columns[feature_index]][i])):
                            F[feature_name]['red_x_points'].append([
                                round(fcs[i][feature_index], 5),
                                F[feature_name]['y_categories'].index(
                                    X[X.columns[feature_index]][i])
                            ])
                        else:
                            F[feature_name]['red_x_points'].append([
                                round(fcs[i][feature_index], 5),
                                len(F[feature_name]['y_categories'])-1
                            ])
                    else:
                        if (not utils.isnan(X[X.columns[feature_index]][i])):
                            F[feature_name]['blue_x_points'].append([
                                round(fcs[i][feature_index], 5),
                                F[feature_name]['y_categories'].index(
                                    X[X.columns[feature_index]][i])
                            ])
                        else:
                            F[feature_name]['blue_x_points'].append([
                                round(fcs[i][feature_index], 5),
                                len(F[feature_name]['y_categories'])-1
                            ])

        else:
            F['attributes'] = {}
            for feature_index in fcs[i].keys():
                feature_name = clf.attributes[feature_index]

                if (feature_name not in F.keys()):
                    if (isinstance(X, pd.DataFrame)):
                        F['attributes'][feature_name] = {
                            'value': X.values[i][feature_index] if not utils.isnan(X.values[i][feature_index]) else 'nan',
                            'contribution': fcs[i][feature_index]
                        }
                    else:
                        F['attributes'][feature_name] = {
                            'value': X[i][feature_index] if not utils.isnan(X[i][feature_index]) else 'nan',
                            'contribution': fcs[i][feature_index]
                        }

    file = open(out, 'w')
    if (addline is not None):
        F['classification'] = addline

    for key in F.keys():
        if type(key) is not str:
            print(key)

    jsonfile = json.dumps(F, ensure_ascii=False)
    file.write(jsonfile)

# class_name="Q92510_snDorPos19"
# with open('prognostic_model_'+ class_name + '.pickle', 'rb') as handle:
#     clf = pickle.load(handle)
# transform_to_JSON(clf, clf.feature_contribution())
