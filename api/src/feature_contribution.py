import pickle
from transform_to_json import transform_to_JSON
from utils import findFiles

def feature_contribution(model_filename,admission_assessment_code,follow_up_assessment_code):
    if(model_filename[-7:] != '.pickle' ):
        model_filename = model_filename + '.pickle'
    try:
        with open(model_filename, 'rb') as handle:
            clf = pickle.load(handle)
    except(FileNotFoundError):
        logging.error('Could not find the model file %r.\n' % model_filename)
        exit()

    if 'yonPexPain' in model_filename:
        class_name = follow_up_assessment_code + '_yonPexPain'
    elif 'lstPexMuscstrength[ShoulderAbduc]' in model_filename:
        class_name = follow_up_assessment_code + '_lstPexMuscstrength[ShoulderAbduc]'
    elif 'lstPexMuscstrength[ElbowFlex]' in model_filename:
        class_name = follow_up_assessment_code + '_lstPexMuscstrength[ElbowFlex]'
    else:
        class_name = follow_up_assessment_code + '_lstPexMuscstrength[ShoulderExrot]'

    file_path = model_filename[:-7]+"_feature_contribution.json"
    transform_to_JSON(clf,clf.feature_contribution(), admission_assessment_code,out= file_path)


if __name__ == '__main__':
    admission_assessment_code = 'QA' #'Q44071'
    follow_up_assessment_code = 'QF' #'Q92510'

    file_paths = findFiles('*.pickle','./models')
    for model_filename in file_paths:
        feature_contribution(model_filename,admission_assessment_code,follow_up_assessment_code)


# class_name="Q92510_snDorPos19"
# with open('prognostic_model_'+ class_name + '.pickle', 'rb') as handle:
#     clf = pickle.load(handle)
# transform_to_JSON(clf,clf.feature_contribution())
