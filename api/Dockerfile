# Use official Python's alpine image
FROM python:3.7.3-slim

# Create API_PATH if it doesn't exist and set it as the working dir
ENV API_PATH=/usr/src/api
WORKDIR ${API_PATH}

# Define environment variables for the API
ENV HOST=0.0.0.0 \
    PORT=5000

# Expose default port to connect with service
EXPOSE ${PORT}

# Define environment variables for Flask
ENV FLASK_APP=${API_PATH}/api.py \
    FLASK_DEBUG=1

# Update and install packages
RUN apt-get update && apt-get install -y \
	  coreutils \
    && apt-get clean

# Copy dependency files
COPY requirements.txt ./

# Install dependencies
RUN pip3 install -r requirements.txt

# Copy the application code to the installation path
COPY . .

# Define default command to execute when running the container
CMD python -m flask run -h ${HOST} -p ${PORT}
