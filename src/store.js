import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import router from "./router.js";

Vue.use(Vuex);

const $axios = axios.create({
  baseURL: process.env.VUE_APP_SERVER_URL
});

export default new Vuex.Store({
  state: {
    modelCategory: "",

    fieldDescriptions: null,
    fieldRelevance: null,

    models: null,
    bestModel: 0,
    selectedModel: 0,

    attributes: null,
    selectedAttribute: "",

    isClassifying: false,
    classifications: null,
    selectedClassification: 0,

    isCleaning: false,

    pageShown: 1,
    errorMessage: ""
  },
  mutations: {
    UPDATE_MODEL_CATEGORY(state, category) {
      state.modelCategory = category;
    },

    UPDATE_FIELD_DESCRIPTIONS(state, fieldDescriptions) {
      state.fieldDescriptions = fieldDescriptions;
    },
    UPDATE_FIELD_RELEVANCE(state, fieldRelevance) {
      state.fieldRelevance = fieldRelevance;
    },

    UPDATE_MODELS(state, models) {
      state.models = models;
    },
    UPDATE_BEST_MODEL(state, bestModel) {
      state.bestModel = bestModel;
    },
    UPDATE_SELECTED_MODEL(state, selectedModel) {
      state.selectedModel = selectedModel;
    },

    UPDATE_ATTRIBUTES(state, attributes) {
      state.attributes = attributes;
    },
    UPDATE_SELECTED_ATTRIBUTE(state, selectedAttribute) {
      state.selectedAttribute = selectedAttribute;
    },

    UPDATE_CLASSIFICATION_STATUS(state, status) {
      state.isClassifying = status;
    },
    UPDATE_CLASSIFICATIONS(state, classifications) {
      state.classifications = classifications;
    },
    UPDATE_SELECTED_CLASSIFICATION(state, selectedClassification) {
      state.selectedClassification = selectedClassification;
    },

    UPDATE_CLEANING_STATUS(state, status) {
      state.isCleaning = status;
    },

    UPDATE_PAGE_SHOWN(state, page) {
      state.pageShown = page;
    },

    UPDATE_ERROR_MESSAGE(state, errorMessage) {
      state.errorMessage = errorMessage;
    }
  },
  actions: {
    async initializeFields({ dispatch }) {
      await dispatch("initializeFieldDescriptions");
    },

    async changeModelCategory({ state, commit, dispatch }, newCategory) {
      if (state.modelCategory === newCategory) return;
      commit("UPDATE_MODEL_CATEGORY", newCategory);
      commit("UPDATE_PAGE_SHOWN", 1);

      await dispatch("initializeModels");
      await dispatch("initializeAttributes");
      await dispatch("initializeFieldRelevance");
    },

    async initializeFieldDescriptions({ commit }) {
      const { data } = await $axios.get(`/field_descriptions`);

      commit("UPDATE_FIELD_DESCRIPTIONS", data);
    },

    async initializeFieldRelevance({ state, commit }) {
      const { data } = await $axios.get(
        `/${state.modelCategory}/field_relevance`
      );

      commit("UPDATE_FIELD_RELEVANCE", data);
    },

    async initializeModels({ state, commit }) {
      const { data } = await $axios.get(
        `/${state.modelCategory}/accuracy_threshold`
      );

      commit("UPDATE_MODELS", data.data);
      commit("UPDATE_BEST_MODEL", data.special);
      commit("UPDATE_SELECTED_MODEL", data.special);
    },
    changeSelectedModel({ commit, dispatch }, selectedModel) {
      commit("UPDATE_SELECTED_MODEL", selectedModel);
      dispatch("initializeAttributes");
    },

    async initializeAttributes({ state, commit }) {
      const { data } = await $axios.get(
        `/${state.modelCategory}/${state.selectedModel}/feature_contributions`
      );

      commit("UPDATE_ATTRIBUTES", data);
      commit("UPDATE_SELECTED_ATTRIBUTE", Object.keys(data)[0]);
      commit("UPDATE_PAGE_SHOWN", 1);
    },
    changeSelectedAttribute({ commit }, selectedAttribute) {
      commit("UPDATE_SELECTED_ATTRIBUTE", selectedAttribute);
      commit("UPDATE_PAGE_SHOWN", 1);
    },

    async classifyPatients({ state, commit }) {
      commit("UPDATE_CLASSIFICATION_STATUS", true);

      try {
        commit("UPDATE_ERROR_MESSAGE", "");

        const { data } = await $axios.post(
          `/${state.modelCategory}/${state.selectedModel}/classify`,
          {
            user_id: router.currentRoute.query.user_id,
            export_id: router.currentRoute.query.export_id
          }
        );

        commit("UPDATE_CLASSIFICATIONS", data);
        commit("UPDATE_PAGE_SHOWN", 2);
      } catch (e) {
        if (e.response) {
          console.log(e.response);
          commit("UPDATE_ERROR_MESSAGE", e.response.data.error);
        } else if (e.request) {
          console.log(e.request);
          commit("UPDATE_ERROR_MESSAGE", e.request);
        } else {
          console.log(e.message);
          commit("UPDATE_ERROR_MESSAGE", e.message);
        }
      }

      commit("UPDATE_CLASSIFICATION_STATUS", false);
    },
    changeSelectedClassification({ commit }, selectedClassification) {
      commit("UPDATE_SELECTED_CLASSIFICATION", selectedClassification);
      commit("UPDATE_PAGE_SHOWN", 3);
    },
    showClassifiedPatients({ commit }) {
      commit("UPDATE_PAGE_SHOWN", 2);
    }
  }
});
