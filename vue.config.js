module.exports = {
  lintOnSave: undefined,
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/assets/tailwind.scss";
        `
      }
    }
  }
};
